[![pipeline status](https://gitlab.com/theodowling/elixir-investec-open-api/badges/master/pipeline.svg)](https://gitlab.com/theodowling/elixir-investec-open-api/-/commits/master)
[![coverage report](https://gitlab.com/theodowling/elixir-investec-open-api/badges/master/coverage.svg)](https://gitlab.com/theodowling/elixir-investec-open-api/-/commits/master)

# Investec Open API

This is a wrapper for the Investec Open API `https://developer.investec.com/programmable-banking/`

[Documentation](https://hexdocs.pm/investec_open_api)

## Please note

The API is currently in public beta, and might change (adding more functionality or changing existing behavior). I will try and keep this updated as changes are released.

Unfortunately I am unable to assist with gaining access to the beta program.

## Installation

The package can be installed by adding `investec_open_api` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:investec_open_api, "~> 0.1.0"}
  ]
end
```

After you added this to your dependencies, remember to run `mix deps.get`

## Considerations

There are two ways to authenticate using this package.

1) you can add your `client_id` and `client_secret` in the config of the application allowing you to start an authenticate client directly with `InvestecOpenApi.new/0`

2) store the `client_id` and `client_secret` somewhere else, and manually use it every time you want to start a new client using `InvestecOpenApi.new(client_id, client_secret)`

## [Optional] Adding `client_id` and `client_secret` in your config

If you want to only authenticate the same account each time, you can add the following to your config:

```elixir
config :investec_open_api,
  client_id: "thisIsmysecretclientId",
  client_secret: "verySuperSecureSecret"
```

## Usage

### Start an authenticated session/client

If you have your `client_id` and `client_secret` in your config (see above for details) you can use this:
```elixir
iex> {:ok, client} = InvestecOpenApi.new()
{:ok,
 %InvestecOpenApi.Client{
   access_token: "your_new_access_token",
   client_id: "thisIsmysecretclientId",
   client_secret: "verySuperSecureSecret",
   expires_at: 1594999999,
   method: :token
 }}
```

Otherwise you can always pass the credentials directly:
```elixir
iex> {:ok, client} = InvestecOpenApi.new(your_client_id, your_client_secret)
{:ok,
 %InvestecOpenApi.Client{
   access_token: "your_new_access_token",
   client_id: "your_client_id",
   client_secret: "your_client_secret",
   expires_at: 1594999999,
   method: :token
 }}
```

You can now use this authenticated client/session to call any of the available methods exposed through the Investec Open API.

### Currently the following methods are available:

### 1. Get Accounts

Use an authenticated client to get the list of accounts

```elixir
# First create a client
iex> {:ok, client} = InvestecOpenApi.new()

# You can then use this client to get list of accounts:
iex> {:ok, accounts, client} = InvestecOpenApi.list_accounts(client)
iex> accounts
[
  %InvestecOpenApi.Accounts{account_id: "172878438321553632224",
  account_name: "Mr John Doe",
  account_number: "10010206147",
  product_name: "Private Bank Account",
  reference_name: "My Investec Private Bank Account"}
]
```

See `InvestecOpenApi.Accounts` for more information

### 2. Get Account Transactions

Use an authenticated client an an account to get list of the transactions on the account

```elixir
# First create a client
iex> {:ok, client} = InvestecOpenApi.new()

# Use this client to get list of accounts - pattern matching on the first one:
iex> {:ok, [account | _], client} = InvestecOpenApi.list_accounts(client)

# Use this client to get list of transactions for the given account:
iex> {:ok, transactions, client} = InvestecOpenApi.list_account_transactions(client, account)

# Get the first transaction
iex> List.first(transactions)
%InvestecOpenApi.Accounts.Transaction{
  account_id: "172878438321553632224",
  action_date: "2020-06-18",
  amount: 535,
  card_number: "",
  description: "MONTHLY SERVICE CHARGE",
  posting_date: "2020-06-11",
  status: "POSTED",
  type: "DEBIT",
  value_date: "2020-06-10"
}
```

See `InvestecOpenApi.Accounts.Transaction` for more information

### 3. Get Account Balance

Use an authenticated client an an account to get the balance of the account

```elixir
# First create a client
iex> {:ok, client} = InvestecOpenApi.new()

# Use this client to get list of accounts - pattern matching on the first one.
iex> {:ok, [account | _], client} = InvestecOpenApi.list_accounts(client)

# Use this client to get the balance for the given account:
iex> {:ok, balance, client} = InvestecOpenApi.get_account_balance(client, account)
iex> balance
%InvestecOpenApi.Accounts.Balance{
  account_id: "172878438321553632224",
  available_balance: 98857.76,
  currency: "ZAR",
  current_balance: 28857.76
}
```

See `InvestecOpenApi.Accounts.Balance` for more information

## Contributing

Thanks for considering contributing to this project, and to the free software ecosystem at large!

Interested in contributing a bug report? Terrific! Please open a GitLab issue and include as much detail as you can. If you have a solution, even better -- please open a pull request with a clear description and tests.

Have a feature idea? Excellent! Please open a GitLab issue for discussion.

Want to implement an issue that's been discussed? Fantastic! Please open a GitLab pull request and write a clear description of the patch. We'll merge your PR a lot sooner if it is well-documented and fully tested.

## License

Copyright © 2020 Theo Dowling

This work is free. You can redistribute it and/or modify it under the
terms of the MIT License. See the LICENSE file for more details.
