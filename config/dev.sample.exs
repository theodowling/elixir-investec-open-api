# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :investec_open_api,
  client_id: "===INSERT INVESTEC OPEN API CLIENT ID HERE ===",
  client_secret: "===INSERT INVESTEC OPEN API CLIENT SECRET HERE ===",
  base_url: "https://openapi.investec.com"
