# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :investec_open_api,
  client_id: "thisIsmysecretclientId",
  client_secret: "verySuperSecureSecret",
  base_url: "https://openapi.investec.com"

config :investec_open_api,
  http_handler: InvestecOpenApi.HTTP.Faker
