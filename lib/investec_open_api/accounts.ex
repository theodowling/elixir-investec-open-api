defmodule InvestecOpenApi.Accounts do
  @moduledoc """
  Methods to obtain account details
  """

  defstruct account_id: "",
            account_name: "",
            account_number: "",
            product_name: "",
            reference_name: ""

  @type t() :: %__MODULE__{
          account_id: binary(),
          account_name: binary(),
          account_number: binary(),
          product_name: binary(),
          reference_name: binary()
        }

  alias InvestecOpenApi.HTTP

  @accounts_endpoint "/za/pb/v1/accounts"

  @doc """
  List all the accounts for a given user. This needs an authenticated `client` as an input parameter.

  It will return a list with `%InvestecOpenApi.Accounts{}` objects.

  With this it also returns an authenticated `%InvestecOpenApi.Client{}` again,
  because if the previous `access_token` has expired, a new one woule automatically be created again

  ## Example
      iex> {:ok, client} = InvestecOpenApi.new()
      ...> {:ok, accounts, _client} = InvestecOpenApi.Accounts.list_accounts(client)
      ...> accounts
      [
        %InvestecOpenApi.Accounts{account_id: "172878438321553632224",
        account_name: "Mr John Doe",
        account_number: "10010206147",
        product_name: "Private Bank Account",
        reference_name: "My Investec Private Bank Account"}
      ]
  """

  @spec list_accounts(InvestecOpenApi.Client.t()) ::
          {:error, binary | Jason.DecodeError.t()} | {:ok, [t()], InvestecOpenApi.Client.t()}
  def list_accounts(client) do
    with {client, {:ok, %HTTPoison.Response{body: body}}} <- call_list_accounts(client),
         {:ok, response} <- Jason.decode(body) do
      {:ok, Enum.map(response["data"]["accounts"], &create_account_object(&1)), client}
    else
      {:error, error} ->
        {:error, error}

      error ->
        {:error, inspect(error)}
    end
  end

  @spec call_list_accounts(InvestecOpenApi.Client.t()) ::
          {InvestecOpenApi.Client.t(), HTTP.response()}
  def call_list_accounts(client), do: HTTP.call(client, :get, @accounts_endpoint)

  @spec create_account_object(map) :: InvestecOpenApi.Accounts.t()
  def create_account_object(account) do
    %__MODULE__{
      account_id: account["accountId"],
      account_name: account["accountName"],
      account_number: account["accountNumber"],
      product_name: account["productName"],
      reference_name: account["referenceName"]
    }
  end
end
