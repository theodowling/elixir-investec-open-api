defmodule InvestecOpenApi.Client do
  @moduledoc """
  This module deals with authenticating storing `access_token` between `api` calls.

  """
  defstruct access_token: "",
            client_id: nil,
            client_secret: nil,
            expires_at: 0,
            method: :unknown

  @type t() :: %__MODULE__{
          access_token: binary(),
          client_id: binary(),
          client_secret: binary(),
          expires_at: integer(),
          method: atom()
        }

  @token_path "/identity/v2/oauth2/token"

  @doc """
  Authenticate returning a session that can be used for subsequent API calls

  Methods:

  1. Using `new/1` to use `client_id` and `client_secret` defined in application config.

  ## Example

      iex> {:ok, client} = InvestecOpenApi.Client.new()
      ...> client.access_token
      "Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"

  Please note that it will raise error if `client_id` and `client_secret` is not found in your config.

  2. Using `new(client_id, client_secret)` to authenticate with specific credentials

  ## Example

      iex> {:ok, client} = InvestecOpenApi.Client.new("thisIsmysecretclientId", "verySuperSecureSecret")
      ...> client.access_token
      "Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"

  3. Using an existing `%Client{}` as parameter. This is useful when you want to re-authenticate when a session has expired.

  ## Example

      iex> {:ok, client} = InvestecOpenApi.Client.new()
      ...> assert client.access_token == "Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"
      ...> {:ok, client} = InvestecOpenApi.Client.new(client)
      ...> client.access_token
      "Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"
  """
  @spec new :: {:error, any} | {:ok, t()}
  def new() do
    username =
      Application.get_env(:investec_open_api, :client_id) ||
        raise("Missing :client_id environment variable")

    password =
      Application.get_env(:investec_open_api, :client_secret) ||
        raise("Missing :client_secret environment variable")

    new(username, password)
  end

  @spec new(binary(), binary()) :: {:error, any} | {:ok, t()}
  def new(username, password) do
    new(%InvestecOpenApi.Client{client_id: username, client_secret: password})
  end

  @spec new(t()) :: {:ok, t()} | {:error, any}
  def new(client) do
    with {client, {:ok, %HTTPoison.Response{body: body}}} <- call_generate_token(client),
         {:ok, %{"access_token" => access_token, "expires_in" => expires_in}} <-
           Jason.decode(body) do
      {:ok,
       client
       |> Map.put(:access_token, access_token)
       |> Map.put(:expires_at, System.os_time(:second) + expires_in)
       |> Map.put(:method, :token)}
    else
      {:error, error} ->
        nil
        {:error, error}

      error ->
        {:error, inspect(error)}
    end
  end

  @spec call_generate_token(t()) :: {InvestecOpenApi.Client.t(), InvestecOpenApi.HTTP.response()}
  def call_generate_token(client) do
    client = Map.put(client, :method, :basic)

    InvestecOpenApi.HTTP.call(
      client,
      :post,
      @token_path,
      URI.encode_query(%{"scope" => "accounts", "grant_type" => "client_credentials"}),
      [{"Content-Type", "application/x-www-form-urlencoded"}]
    )
  end

  @doc """
  Automatically check if the `access_token` is expired or not. If it is expired,
  a new session will automatically be started.

  ## Example

      iex> client = InvestecOpenApi.Client.validate_access_token_still_valid(%InvestecOpenApi.Client{
      ...>    access_token: "oldExpiredToken",
      ...>    client_id: "thisIsmysecretclientId",
      ...>    client_secret: "verySuperSecureSecret",
      ...>    expires_at: 0, # Simulating an expired token
      ...>    method: :token
      ...> })
      ...> refute client.access_token ==  "oldExpiredToken"
      ...> client_update = InvestecOpenApi.Client.validate_access_token_still_valid(client)
      ...> assert client_update.access_token == client.access_token
      ...> client.access_token
      "Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"

  """

  @spec validate_access_token_still_valid(t()) :: t()
  def validate_access_token_still_valid(client) do
    if client.expires_at >= System.os_time(:second) + 60 do
      client
    else
      {:ok, client} = new(client)
      client
    end
  end
end
