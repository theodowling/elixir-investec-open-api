defmodule InvestecOpenApi.HTTP.Faker do
  alias InvestecOpenApi.HTTP
  @behaviour HTTP
  @moduledoc """
  This module used to "mock" all the api calls, so that we have deterministic tests.
  """

  @doc """
  Making http call using basic-auth
  """
  @impl HTTP
  def request(
        :post,
        "https://openapi.investec.com/identity/v2/oauth2/token",
        "grant_type=client_credentials&scope=accounts",
        [
          {"Authorization", "Basic dGhpc0lzbXlzZWNyZXRjbGllbnRJZDp2ZXJ5U3VwZXJTZWN1cmVTZWNyZXQ="},
          {"Content-Type", "application/x-www-form-urlencoded"}
        ],
        ssl: [versions: [:"tlsv1.2"]],
        recv_timeout: 30_000
      ) do
    {:ok,
     %HTTPoison.Response{
       body:
         "{\"access_token\": \"Ms9OsZkyrhBZd5yQJgfEtiDy4t2c\",\"token_type\": \"Bearer\",\"expires_in\": 1799,\"scope\": \"accounts\"}",
       request: %HTTPoison.Request{
         body: "grant_type=client_credentials&scope=accounts",
         headers: [
           {"Authorization",
            "Basic dGhpc0lzbXlzZWNyZXRjbGllbnRJZDp2ZXJ5U3VwZXJTZWN1cmVTZWNyZXQ="},
           {"Content-Type", "application/x-www-form-urlencoded"}
         ],
         method: :post,
         options: [],
         params: %{},
         url: "https://openapi.investec.com/identity/v2/oauth2/token"
       },
       request_url: "https://openapi.investec.com/identity/v2/oauth2/token",
       status_code: 200
     }}
  end

  def request(
        :get,
        "https://openapi.investec.com/za/pb/v1/accounts",
        "",
        [{"Authorization", "Bearer Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"}],
        ssl: [versions: [:"tlsv1.2"]],
        recv_timeout: 30_000
      ) do
    {:ok,
     %HTTPoison.Response{
       body:
         "{\"data\": {\"accounts\": [{\"accountId\": \"172878438321553632224\",\"accountNumber\": \"10010206147\",\"accountName\": \"Mr John Doe\",\"referenceName\": \"My Investec Private Bank Account\",\"productName\": \"Private Bank Account\"}]},\"links\": {\"self\": \"https://openapi.investec.com/za/pb/v1/accounts\"},\"meta\": {\"totalPages\": 1}}",
       request: %HTTPoison.Request{
         body: "",
         headers: [{"Authorization", "Bearer Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"}],
         method: :get,
         options: [],
         params: %{},
         url: "https://openapi.investec.com/za/pb/v1/accounts"
       },
       request_url: "https://openapi.investec.com/za/pb/v1/accounts",
       status_code: 200
     }}
  end

  def request(
        :get,
        "https://openapi.investec.com/za/pb/v1/accounts/172878438321553632224/transactions",
        "",
        [{"Authorization", "Bearer Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"}],
        ssl: [versions: [:"tlsv1.2"]],
        recv_timeout: 30_000
      ) do
    {:ok,
     %HTTPoison.Response{
       body:
         "{\"data\": {\"transactions\": [{\"accountId\": \"172878438321553632224\",\"type\": \"DEBIT\",\"status\": \"POSTED\",\"description\": \"MONTHLY SERVICE CHARGE\",\"cardNumber\": \"\",\"postingDate\": \"2020-06-11\",\"valueDate\": \"2020-06-10\",\"actionDate\": \"2020-06-18\",\"amount\": 535},{\"accountId\": \"172878438321553632224\",\"type\": \"CREDIT\",\"status\": \"POSTED\",\"description\": \"CREDIT INTEREST\",\"cardNumber\": \"\",\"postingDate\": \"2020-06-11\",\"valueDate\": \"2020-06-10\",\"actionDate\": \"2020-06-18\",\"amount\": 31.09}]},\"links\": {\"self\": \"https://openapi.investec.com/za/pb/v1/accounts/{accountId}/transactions\"},\"meta\": {\"totalPages\": 1}}",
       request: %HTTPoison.Request{
         body: "",
         headers: [{"Authorization", "Bearer Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"}],
         method: :get,
         options: [],
         params: %{},
         url: "https://openapi.investec.com/za/pb/v1/accounts/172878438321553632224/transactions"
       },
       request_url:
         "https://openapi.investec.com/za/pb/v1/accounts/172878438321553632224/transactions",
       status_code: 200
     }}
  end

  def request(
        :get,
        "https://openapi.investec.com/za/pb/v1/accounts/172878438321553632224/balance",
        "",
        [{"Authorization", "Bearer Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"}],
        ssl: [versions: [:"tlsv1.2"]],
        recv_timeout: 30_000
      ) do
    {:ok,
     %HTTPoison.Response{
       body:
         "{\"data\": {\"accountId\": \"172878438321553632224\",\"currentBalance\": 28857.76,\"availableBalance\": 98857.76,\"currency\": \"ZAR\"},\"links\": {\"self\": \"https://openapi.investec.com/za/pb/v1/accounts/{accountId}/balance\"},\"meta\": {\"totalPages\": 1}}",
       request: %HTTPoison.Request{
         body: "",
         headers: [{"Authorization", "Bearer Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"}],
         method: :get,
         options: [],
         params: %{},
         url: "https://openapi.investec.com/za/pb/v1/accounts/172878438321553632224/balance"
       },
       request_url:
         "https://openapi.investec.com/za/pb/v1/accounts/172878438321553632224/balance",
       status_code: 200
     }}
  end

  # @doc """
  # This is to log all http calls that aren't dealt with to assist with development and making sure that all http calls are faked.
  # """
  # def request(method, path, body, headers, options) do
  #   IO.inspect({method, path, body, headers, options}, label: "Unhandled faker http call")
  # end
end
