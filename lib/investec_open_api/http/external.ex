defmodule InvestecOpenApi.HTTP.External do
  alias InvestecOpenApi.HTTP
  @behaviour HTTP
  @moduledoc """
  This is the boundary to connect to the outside world. And is mainly done so that http calls can be mocked properly for testing purposes.
  """

  @doc """
  Making http call to the outside world.
  If we want to make use of a different `http` library then, we can update it at this point.
  """
  @impl HTTP
  def request(method, path, body, headers, options) do
    HTTPoison.request(method, path, body, headers, options)
  end
end
