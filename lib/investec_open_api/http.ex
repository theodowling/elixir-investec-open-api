defmodule InvestecOpenApi.HTTP do
  @moduledoc """
  ## InvestecOpenApi.HTTP

  This module is used to prepare for the `http` calls to the outside world.
  It makes use of HTTPoison, but can easily be swopped out with a different HTTP client if required.

  `http_handler` is defined in test config to use the `InvestecOpenApi.HTTP.Faker` module,
  otherwise it will default to using `InvestecOpenApi.HTTP.External`.
  """

  @type method :: :get | :post | :put | :patch | :delete | :options | :head
  @type headers :: [{atom, binary}] | [{binary, binary}] | %{binary => binary} | any
  @type response :: {:ok, HTTPoison.Response.t()} | {:error, HTTPoison.Error.t()}

  @callback request(method(), binary, any, headers(), keyword) :: response()

  @http_handler Application.get_env(:investec_open_api, :http_handler) ||
                  InvestecOpenApi.HTTP.External

  @base_url Application.get_env(:investec_open_api, :base_url) ||
              "https://openapi.investec.com"

  @doc """
  The main `InvestecOpenApi.HTTP.call/5` method deals with preparing authentication headers,
  and then making the call to the api endpoint over HTTP using config defined `http_handler`.

  ## Parameters:
  - `%InvestecOpenApi.Client{}` which has the details to start an authenticated session or us an existing `access_token`
  - `method` will be the HTTP verb to use. Currently either `:get` or `:post`
  - `path` relative http path for the endpoint to call. This will be relative of the config definded `:investec_open_api, :base_url`
  - `body` (optional) - body to send with the http call
  - `headers` (optional) - additional http headers required (over and above authentication headers)

  ## Example

      iex> {:ok, client} = InvestecOpenApi.Client.new()
      ...> {client, {:ok, %HTTPoison.Response{body: body}}} = InvestecOpenApi.HTTP.call(client, :get, "/za/pb/v1/accounts", "", [])
      ...> Jason.decode!(body)
      %{"data" => %{"accounts" => [%{"accountId" => "172878438321553632224", "accountName" => "Mr John Doe", "accountNumber" => "10010206147", "productName" => "Private Bank Account", "referenceName" => "My Investec Private Bank Account"}]}, "links" => %{"self" => "https://openapi.investec.com/za/pb/v1/accounts"}, "meta" => %{"totalPages" => 1}}
  """
  @spec call(InvestecOpenApi.Client.t(), method, binary, String.t(), headers) ::
          {InvestecOpenApi.Client.t(), response}
  def call(client, method, path, body \\ "", headers \\ []) do
    {client, headers} = prepare_headers(client, headers)

    {client,
     @http_handler.request(method, @base_url <> path, body, headers,
       ssl: [{:versions, [:"tlsv1.2"]}],
       recv_timeout: 30_000
     )}
  end

  @doc """
  `prepare_headers/2` understands how to add the `Authorization` header entry for different Api calls.
  Some of the calls require `basic-auth` and others authenticate using a `bearer-token`.

  It will also check for validity of the `access_token` and if it is close to expiry (within 60 seconds)
  then a new token will be requested - see `InvestecOpenApi.Client.validate_access_token_still_valid/1` for more details

  """
  @spec prepare_headers(InvestecOpenApi.Client.t(), headers) ::
          {InvestecOpenApi.Client.t(), headers}
  def prepare_headers(
        %{client_id: username, client_secret: password, method: :basic} = client,
        headers
      ) do
    {client, [basic_auth_header(username, password) | headers]}
  end

  def prepare_headers(%{method: :token} = client, headers) do
    updated_client = InvestecOpenApi.Client.validate_access_token_still_valid(client)
    {updated_client, [bearer_token_header(updated_client) | headers]}
  end

  defp basic_auth_header(username, password) do
    {"Authorization", "Basic " <> Base.encode64("#{username}:#{password}")}
  end

  defp bearer_token_header(%{access_token: access_token}) do
    {"Authorization", "Bearer " <> access_token}
  end
end
