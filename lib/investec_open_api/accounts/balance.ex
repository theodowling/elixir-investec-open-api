defmodule InvestecOpenApi.Accounts.Balance do
  @moduledoc """
  This module deals with the get_account_balance api endpoint.
  """

  defstruct account_id: "",
            available_balance: 0.0,
            currency: "",
            current_balance: 0.0

  @type t() :: %__MODULE__{
          account_id: binary(),
          available_balance: number(),
          currency: binary(),
          current_balance: number()
        }

  @accounts_endpoint "/za/pb/v1/accounts"
  alias InvestecOpenApi.HTTP

  @doc """
  Outputs the balance for a given user and account. This needs an authenticated `client`
  and `account_id` (or `%InvestecOpenApi.Accounts{}` object) as input parameters.

  It will an `%InvestecOpenApi.Accounts.Balance{}` object.

  With this it also returns an authenticated `%InvestecOpenApi.Client{}` again,
  because if the previous `access_token` has expired, a new one woule automatically be created again

  ## Example

      iex> {:ok, client} = InvestecOpenApi.new()
      ...> {:ok, [account | _], client} = InvestecOpenApi.Accounts.list_accounts(client)
      ...> {:ok, balance, client} = InvestecOpenApi.Accounts.Balance.get_balance(client, account)
      ...> balance
      %InvestecOpenApi.Accounts.Balance{
        account_id: "172878438321553632224",
        available_balance: 98857.76,
        currency: "ZAR",
        current_balance: 28857.76
      }
  """

  @spec get_balance(InvestecOpenApi.Client.t(), binary | InvestecOpenApi.Accounts.t()) ::
          {:error, any}
          | {:ok, InvestecOpenApi.Accounts.Balance.t(), InvestecOpenApi.Client.t()}

  def get_balance(client, %{account_id: account_id}) when not is_nil(account_id) do
    get_balance(client, account_id)
  end

  def get_balance(client, account_id) when is_binary(account_id) do
    with {client, {:ok, %HTTPoison.Response{body: body}}} <-
           call_get_balance(client, account_id),
         {:ok, response} <- Jason.decode(body) do
      {:ok, create_balance_object(response["data"]), client}
    else
      {:error, error} ->
        {:error, error}

      error ->
        {:error, inspect(error)}
    end
  end

  @spec call_get_balance(InvestecOpenApi.Client.t(), binary()) ::
          {InvestecOpenApi.Client.t(),
           {:error, HTTPoison.Error.t()} | {:ok, HTTPoison.Response.t()}}
  def call_get_balance(client, account_id),
    do: HTTP.call(client, :get, Path.join([@accounts_endpoint, account_id, "balance"]))

  @spec create_balance_object(nil | map) ::
          InvestecOpenApi.Accounts.Balance.t()
  def create_balance_object(balance) do
    %__MODULE__{
      account_id: balance["accountId"],
      available_balance: balance["availableBalance"],
      currency: balance["currency"],
      current_balance: balance["currentBalance"]
    }
  end
end
