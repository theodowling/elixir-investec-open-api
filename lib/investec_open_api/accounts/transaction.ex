defmodule InvestecOpenApi.Accounts.Transaction do
  @moduledoc """
  This module deals with listing the transactions for a specific account
  """

  defstruct account_id: "",
            action_date: "",
            amount: 0.00,
            card_number: "",
            description: "",
            posting_date: "",
            status: "",
            type: "",
            value_date: ""

  @type t() :: %__MODULE__{
          account_id: binary(),
          action_date: binary(),
          amount: number(),
          card_number: binary(),
          description: binary(),
          posting_date: binary(),
          status: binary(),
          type: binary(),
          value_date: binary()
        }

  @accounts_endpoint "/za/pb/v1/accounts"
  alias InvestecOpenApi.HTTP

  @doc """
  List all the transactions for a given user and account. This needs an authenticated `client`
  and `account_id` (or `%InvestecOpenApi.Accounts{}` object) as input parameters.

  It will return a list with `%InvestecOpenApi.Accounts.Transaction{}` objects.

  With this it also returns an authenticated `%InvestecOpenApi.Client{}` again,
  because if the previous `access_token` has expired, a new one woule automatically be created again

  ## Example

      iex> {:ok, client} = InvestecOpenApi.new()
      ...> {:ok, [account | _], client} = InvestecOpenApi.Accounts.list_accounts(client)
      ...> {:ok, transactions, client} = InvestecOpenApi.Accounts.Transaction.list_transactions(client, account)
      ...> List.first(transactions)
      %InvestecOpenApi.Accounts.Transaction{
        account_id: "172878438321553632224",
        action_date: "2020-06-18",
        amount: 535,
        card_number: "",
        description: "MONTHLY SERVICE CHARGE",
        posting_date: "2020-06-11",
        status: "POSTED",
        type: "DEBIT",
        value_date: "2020-06-10"
      }
  """

  @spec list_transactions(InvestecOpenApi.Client.t(), binary | InvestecOpenApi.Accounts.t()) ::
          {:error, binary | Jason.DecodeError.t()} | {:ok, [t()], InvestecOpenApi.Client.t()}
  def list_transactions(client, %{account_id: account_id}) when not is_nil(account_id) do
    list_transactions(client, account_id)
  end

  def list_transactions(client, account_id) do
    with {client, {:ok, %HTTPoison.Response{body: body}}} <-
           call_list_transactions(client, account_id),
         {:ok, response} <- Jason.decode(body) do
      {:ok, Enum.map(response["data"]["transactions"], &create_transaction_object(&1)), client}
    else
      {:error, error} ->
        {:error, error}

      error ->
        {:error, inspect(error)}
    end
  end

  @spec call_list_transactions(InvestecOpenApi.Client.t(), binary) ::
          {InvestecOpenApi.Client.t(), HTTP.response()}
  def call_list_transactions(client, account_id),
    do: HTTP.call(client, :get, Path.join([@accounts_endpoint, account_id, "transactions"]))

  @spec create_transaction_object(map) ::
          InvestecOpenApi.Accounts.Transaction.t()
  def create_transaction_object(transaction) do
    %__MODULE__{
      account_id: transaction["accountId"],
      action_date: transaction["actionDate"],
      amount: transaction["amount"],
      card_number: transaction["cardNumber"],
      description: transaction["description"],
      posting_date: transaction["postingDate"],
      status: transaction["status"],
      type: transaction["type"],
      value_date: transaction["valueDate"]
    }
  end
end
