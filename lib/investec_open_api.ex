defmodule InvestecOpenApi do
  @moduledoc """
  Documentation for `InvestecOpenApi`.
  Refer to documentation if anything is unclear: https://developer.investec.com/programmable-banking
  """

  @doc """
  Create an authenticated `client` by requesting a new `access_token`
  This is represented by the `%InvestecOpenApi.Client{}` object.

  ## Example

      iex> # First create a client
      ...> {:ok, client} = InvestecOpenApi.new()
      ...> # You can then get the `access_token` by going:
      ...> client.access_token
      "Ms9OsZkyrhBZd5yQJgfEtiDy4t2c"

  See `InvestecOpenApi.Client` for more information
  """

  @spec new :: {:error, any} | {:ok, InvestecOpenApi.Client.t()}
  defdelegate new, to: InvestecOpenApi.Client

  @doc """
  Use an authenticated client to get the list of accounts

  ## Example

      iex> # First create a client
      ...> {:ok, client} = InvestecOpenApi.new()
      ...> # You can then use this client to get list of accounts:
      ...> {:ok, accounts, client} = InvestecOpenApi.list_accounts(client)
      ...> accounts
      [
        %InvestecOpenApi.Accounts{account_id: "172878438321553632224",
        account_name: "Mr John Doe",
        account_number: "10010206147",
        product_name: "Private Bank Account",
        reference_name: "My Investec Private Bank Account"}
      ]

  See `InvestecOpenApi.Accounts` for more information
  """

  @spec list_accounts(InvestecOpenApi.Client.t()) ::
          {:error, binary | Jason.DecodeError.t()}
          | {:ok, [InvestecOpenApi.Accounts.t()], InvestecOpenApi.Client.t()}
  defdelegate list_accounts(client), to: InvestecOpenApi.Accounts

  @doc """
  Use an authenticated client an an account to get list of the transactions on the account

  ## Example

      iex> # First create a client
      ...> {:ok, client} = InvestecOpenApi.new()
      ...> # Use this client to get list of accounts:
      ...> {:ok, [account | _], client} = InvestecOpenApi.list_accounts(client)
      ...> {:ok, transactions, client} = InvestecOpenApi.list_account_transactions(client, account)
      ...> # Get the first transaction
      ...> List.first(transactions)
      %InvestecOpenApi.Accounts.Transaction{
        account_id: "172878438321553632224",
        action_date: "2020-06-18",
        amount: 535,
        card_number: "",
        description: "MONTHLY SERVICE CHARGE",
        posting_date: "2020-06-11",
        status: "POSTED",
        type: "DEBIT",
        value_date: "2020-06-10"
      }

  See `InvestecOpenApi.Accounts.Transaction` for more information
  """

  @spec list_account_transactions(
          InvestecOpenApi.Client.t(),
          binary | %InvestecOpenApi.Accounts{}
        ) ::
          {:error, binary | Jason.DecodeError.t()}
          | {:ok, [InvestecOpenApi.Accounts.Transaction.t()], InvestecOpenApi.Client.t()}
  defdelegate list_account_transactions(client, account_id),
    to: InvestecOpenApi.Accounts.Transaction,
    as: :list_transactions

  @doc """
  Use an authenticated client an an account to get the balance of the account

  ## Example

      iex> # First create a client
      ...> {:ok, client} = InvestecOpenApi.new()
      ...> # Use this client to get list of accounts:
      ...> {:ok, [account | _], client} = InvestecOpenApi.list_accounts(client)
      ...> {:ok, balance, client} = InvestecOpenApi.get_account_balance(client, account)
      ...> balance
      %InvestecOpenApi.Accounts.Balance{
        account_id: "172878438321553632224",
        available_balance: 98857.76,
        currency: "ZAR",
        current_balance: 28857.76
      }

  See `InvestecOpenApi.Accounts.Balance` for more information
  """
  @spec get_account_balance(InvestecOpenApi.Client.t(), binary) ::
          {:error, binary | Jason.DecodeError.t()}
          | {:ok, InvestecOpenApi.Accounts.Balance.t(), InvestecOpenApi.Client.t()}
  defdelegate get_account_balance(client, account_id),
    to: InvestecOpenApi.Accounts.Balance,
    as: :get_balance
end
