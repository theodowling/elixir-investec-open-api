defmodule InvestecOpenApi.MixProject do
  use Mix.Project

  def project do
    [
      app: :investec_open_api,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      name: "Investec Open API",
      source_url: "https://gitlab.com/theodowling",
      docs: [
        main: "InvestecOpenApi",
        extras: ["README.md"]
      ],
      test_coverage: [tool: ExCoveralls],
      package: package(),
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.7"},
      {:jason, "~> 1.2"},
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.22", only: :dev, runtime: false},
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end

  defp description() do
    "This is an elixir wrapper for the Investec Open API
     allowing for the integration with the api - https://developer.investec.com/programmable-banking/."
  end

  defp package() do
    [
      # This option is only needed when you don't want to use the OTP application name
      name: "investec_open_api",
      # These are the default files included in the package
      files: ~w(lib .formatter.exs mix.exs README* LICENSE*),
      licenses: ["MIT License"],
      links: %{"GitLab" => "https://gitlab.com/theodowling/elixir-investec-open-api"}
    ]
  end
end
